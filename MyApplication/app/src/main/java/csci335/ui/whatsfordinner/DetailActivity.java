package csci335.ui.whatsfordinner;

import android.os.Bundle;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import java.text.DecimalFormat;

import dataclasses.CuisineType;
import dataclasses.Restaurant;
import dataclasses.RestaurantSelector;

public class DetailActivity extends AppCompatActivity {

    TextView tvName;
    TextView tvPrice;
    TextView tvDiningMode;
    TextView tvCuisineType;
    TextView tvThink;
    RestaurantSelector selector = new RestaurantSelector();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvName = (TextView)findViewById(R.id.tvName);
        tvPrice = (TextView)findViewById(R.id.tvPrice);
        tvDiningMode = (TextView)findViewById(R.id.tvDiningMode);
        tvCuisineType = (TextView)findViewById(R.id.tvCuisineType);
        tvThink = (TextView)findViewById(R.id.tvThink);
        tvThink.setVisibility(View.VISIBLE);

        boolean random = getIntent().getExtras().getBoolean("random");
        if(random)
        {
            ShowRestaurant(selector.ChooseRandomRestaurant());
        }
        else
        {
            boolean loadById = getIntent().getExtras().getBoolean("loadById");
            if(loadById)
            {
                tvThink.setVisibility(View.GONE);
                int restaurantId = getIntent().getExtras().getInt("restaurantId");
                ShowRestaurant(selector.getRestaurantById(restaurantId));
                return;
            }

            CuisineType ct = new CuisineType();
            ct.seafood = getIntent().getExtras().getBoolean("seafood");
            ct.breakfast = getIntent().getExtras().getBoolean("breakfast");
            ct.chinese = getIntent().getExtras().getBoolean("chinese");
            ct.italian = getIntent().getExtras().getBoolean("italian");
            ct.mexican = getIntent().getExtras().getBoolean("mexican");
            ct.vietnamese = getIntent().getExtras().getBoolean("vietnamese");

            Restaurant r = selector.ChooseRestaurant(getIntent().getExtras().getInt("price"),
                    getIntent().getExtras().getString("dining"), ct);

            ShowRestaurant(r);
        }

    }

    private void ShowRestaurant(Restaurant restaurant)
    {
        tvName.setText(restaurant.Name);
        DecimalFormat formatter = new DecimalFormat("$ 0.00");

        tvPrice.setText(formatter.format(restaurant.Price));
        tvCuisineType.setText(restaurant.Type.GetCuisineName());
        tvDiningMode.setText(restaurant.Mode);
    }

}
