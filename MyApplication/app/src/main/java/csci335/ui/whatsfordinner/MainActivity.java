package csci335.ui.whatsfordinner;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;


import dataclasses.RestaurantSelector;

public class MainActivity extends AppCompatActivity {

    RestaurantSelector selector = new RestaurantSelector();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ListView lvRest = (ListView) findViewById(R.id.lvRestaurants);
        SimpleAdapter RestAdapter = new SimpleAdapter(this,
                                                        selector.getRestaurantList(),
                                                        android.R.layout.simple_list_item_2,
                                                        new String[] {"name", "type"},
                                                        new int[] {android.R.id.text1,
                                                                   android.R.id.text2});
        lvRest.setAdapter(RestAdapter);
        lvRest.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra("loadById", true);
                intent.putExtra("restaurantId",position);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_randomize) {
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra("random", true);
            startActivity(intent);
        } else if (id == R.id.action_search) {
            Intent intent = new Intent(this, QuestionActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
